/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_1;

import java.util.*;
import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class Bat_tap_hang_ngay {
    public static void menu(){
        System.out.println("1 : sap xep theo thu tu tang dan !");
        System.out.println("2 : sap xep theo thu tu giam dan !");
        System.out.println("3 : sap xep sao cho 2 so chan ko dung canh nhau , 2 so le ko dung canh nhau !");
        System.out.println("4 : kiem tra ton tai cua 1 so va xuat hien bao nhieu lan !");
    }
    
    public static void increase_sort(ArrayList list){
        System.out.println("mang sau khi sap xep tang dan la : ");
        Collections.sort(list);
        for(int i =0 ; i< list.size() ; i++){
            System.out.print(list.get(i) + "  ");
        }
    
    }
    
    public static void decrease_sort(ArrayList list){
        System.out.println("mang sau khi sap xep giam dan la : ");
        Collections.sort(list);
        for(int i =0 ,j = list.size()-1;i<list.size()/2 ;i++, j--){
            int x = (int) list.get(i);
            list.set(i , list.get(j));
            list.set(j, x);
        }
        
        for(int i = 0;i<list.size() ; i++){
            System.out.println(list.get(i));
        }
    }
    
    public static void check_appear(ArrayList list,int x){
        int dem = 0;
        if(list.contains(x)){
            for(int i =0 ;i<list.size();i++){
                if((int)list.get(i) == x){
                    dem++;
                }
            }
            System.out.println("phan tu " + x + "xuat hien trong arraylist "+ dem+ " lan");
        }
        else{
            System.out.println("phan tu ko xuat hien trong arrayList5"
                    + "");
        }
        
    }
    
    public static void even_odd_sort(ArrayList<Integer> list){
        ArrayList<Integer> list_temp_even = new ArrayList<>();
        ArrayList<Integer> list_temp_odd = new ArrayList<>();
        ArrayList<Integer> list_temp_result = new ArrayList<>();
        for(int i=0;i<list.size();i++ ){
            if((int)list.get(i) % 2 ==0){
                list_temp_even.add((int)list.get(i));
            }
            else{
                list_temp_odd.add((int)list.get(i));
            }
        }
        while(list_temp_even.size() != list_temp_odd.size()){
            if(list_temp_even.size() < list_temp_odd.size()){
                list_temp_even.add(0);
            }
            else{
                list_temp_odd.add(0);
            }
        }

        
        
        int a =0 ;
        for(int i =0 ;i< list_temp_even.size() + list_temp_odd.size() ;){
            list_temp_result.add(list_temp_even.get(a));
            i++;
            list_temp_result.add(list_temp_odd.get(a));
            i++;
            a++;
        }
        
        for(int i = 0 ; i < list_temp_result.size() ; i++){
            System.out.print( list_temp_result.get(i)+ "   "); 
        }
        
    }
           

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Nhap so phan tu cho mang : ");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Integer> list = new ArrayList<>();
        for(int i= 0 ; i<n;i++){
            int x = sc.nextInt();
            list.add(x);
        }
        
        System.out.println("array list ma ban vua nhap la : ");
        for(int i =0 ;i<list.size() ;i++){
            System.out.print(list.get(i) + "  ");
        }
        System.out.println("");
        menu();
        int check = sc.nextInt();
        switch(check){
            case 1 : {
                increase_sort(list);
                break ;
            }
            case 2: {
                decrease_sort(list);
                break;
            }
            case 3:{
                even_odd_sort(list);
                break;
            }
            case 4: {
                int x = sc.nextInt();
                check_appear(list,x);
                break ;
                
            }
        }
        
    }
    
}
