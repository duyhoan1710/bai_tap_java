/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_11.bai_2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Dell
 */
public class main {
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("C:\\Users\\Dell\\Documents\\NetBeansProjects\\bai_tap_java\\bat_tap_hang_ngay\\src\\bai_tap_buoi_11\\bai_2\\bai2.json");
        Object obj = jsonParser.parse(reader);
        JSONObject parserObj = (JSONObject) obj;
        long id =(long) parserObj.get("id");
        System.out.println(id);
        String sku = (String) parserObj.get("sku");
        System.out.println(sku);
        String name = (String) parserObj.get("name");
        System.out.println(name);
        String created_at = (String) parserObj.get("created_at");
        System.out.println(created_at);
        String updated_at =  (String) parserObj.get("updated_at");
        System.out.println(updated_at);
        JSONArray product_links = new JSONArray();
        product_links = (JSONArray) parserObj.get("product_links");
        product_links.forEach(product ->parseProduct_links((JSONObject) product));
        
    }

    private static void parseProduct_links(JSONObject product) {
        String selection_id = (String) product.get("selection_id");
        System.out.println(selection_id);
        String qty = (String) product.get("qty");
        System.out.println(qty);
        boolean canApplyMsrp = (boolean) product.get("canApplyMsrp");
        System.out.println(canApplyMsrp);
        String image = (String) product.get("image");
        System.out.println(image);
        JSONObject prices = (JSONObject) product.get("prices");
        
        JSONObject oldPrice  = (JSONObject) prices.get("oldPrice");
        long amount_one = (long) oldPrice.get("amount");
        System.out.println(amount_one);
        JSONObject basePrice = (JSONObject) prices.get("basePrice");
        long amount_two = (long) basePrice.get("amount");
        System.out.println(amount_two);
        JSONObject finalPrice = (JSONObject) prices.get("finalPrice");
        long amount_three = (long) finalPrice.get("amount");
        System.out.println(amount_three);
    }
}
