/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_11.bai_1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 *
 * @author Dell
 */
public class main {
    public static void main(String[] args) {
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader reader = new FileReader("C:\\Users\\Dell\\Documents\\NetBeansProjects\\bai_tap_java\\bat_tap_hang_ngay\\src\\bai_tap_buoi_11\\bai_1\\bai1.json");
            Object obj = jsonParser.parse(reader);
            JSONObject parserObj = (JSONObject) obj;
            long height = (long) parserObj.get("height");
            String chain = (String) parserObj.get("chain");
            long size = (long) parserObj.get("size");
            String time = (String) parserObj.get("time");
            String received_time = (String) parserObj.get("received_time");
            
            
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inputFormat.parse(received_time);
            String formattedDate = outputFormat.format(date);
            
            JSONObject newObject = new JSONObject();
            newObject.put("height", height);
            newObject.put("chain", chain);
            newObject.put("size", size);
            newObject.put("received_time", formattedDate);
            
            System.out.println(newObject);
         
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (java.text.ParseException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
