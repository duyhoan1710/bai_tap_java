/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_vn_buoi_10.bai_3;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Dell
 */
public class main {
    public  static HashMap<String , Object> addData(HashMap<String , Object> map){
        
        ArrayList<lesson> list = new ArrayList<>();
        list.add(new lesson("13,14,15,16",
                    "An toàn hệ điều hành",
                    " 203_TA2 TA2"));
        list.add(new lesson( "10,11,12",
                    "Mã độc",
                    " 303_TA2 TA2"));
        list.add(new lesson("1,2,3",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
        list.add(new lesson("4,5,6",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
        list.add(new lesson("13,14,15,16",
                    "Tiếng Anh 3",
                    " 104_TA1 TA1"));
        
        map.put("1572307200000" , list.toString());
        
        ArrayList<lesson> list_1 = new ArrayList<>();
        list_1.add(new lesson("13,14,15,16",
                    "An toàn hệ điều hành",
                    " 203_TA2 TA2"));
        list_1.add(new lesson("10,11,12",
                    "Mã độc",
                     " 303_TA2 TA2"));
        list_1.add(new lesson("7,8,9",
                    "Mật mã ứng dụng trong an toàn thông tin",
                     " 303_TA2 TA2"));
        list_1.add(new lesson("1,2,3",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
        list_1.add(new lesson("4,5,6",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
        list_1.add(new lesson("13,14,15,16",
                    "Tiếng Anh 3",
                    " 104_TA1 TA1"));
        map.put("1572912000000", list_1.toString());
        
         ArrayList<lesson> list_2 = new ArrayList<>();
         
         list_2.add(new lesson("13,14,15,16",
                    "An toàn hệ điều hành",
                    " 203_TA2 TA2"));
         list_2.add(new lesson( "10,11,12",
                    "Mã độc",
                     " 303_TA2 TA2"));
         list_2.add(new lesson("7,8,9",
                    "Mật mã ứng dụng trong an toàn thông tin",
                     " 303_TA2 TA2"));
         list_2.add(new lesson("1,2,3",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
         list_2.add(new lesson("4,5,6",
                    "Thực hành vật lý đại cương 1&2",
                    " THVL-TA4 TA4"));
         list_2.add(new lesson( "13,14,15,16",
                    "Tiếng Anh 3",
                    " 104_TA1 TA1"));
         
         map.put("1572912000000"  , list_2.toString());
         return map;
    }
    public static void main(String[] args) {
        HashMap<String , Object> map =new HashMap<>();
        map = addData(map);
        sort(map);
        
        fixMap(map);
    }

    private static void sort(HashMap<String, Object> map) {
        Calendar calendar = Calendar.getInstance();  
        TreeMap<String , Object > sortMap = new TreeMap<String , Object>(map);
        sortMap.forEach((k ,v)->{
        calendar.setTimeInMillis(Long.valueOf(k));
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println( "key : " + mDay  + "/" + mMonth + "/" + mYear + " value : " + v);
       });
    }

    private static void fixMap(HashMap<String, Object> map) {
        
    }
}
