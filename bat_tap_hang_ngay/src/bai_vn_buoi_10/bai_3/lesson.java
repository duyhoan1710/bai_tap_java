/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_vn_buoi_10.bai_3;

/**
 *
 * @author Dell
 */
public class lesson {
    private String lesson ;
    private String name;
    private String adress;

    public lesson() {
    }

    public lesson(String lesson, String name, String adress) {
        this.lesson = lesson;
        this.name = name;
        this.adress = adress;
    }

    public String getLesson() {
        return lesson;
    }
 
    public void setLesson(String lesson) {
        this.lesson = lesson;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getAdress() {
        return adress;
    }
 
    public void setAdress(String adress) {
        this.adress = adress;
    }
 
    @Override
    public String toString() {
        return "lesson{" + "lesson=" + lesson + ", name=" + name + ", adress=" + adress + '}';
    }
    
    
    
    
}
