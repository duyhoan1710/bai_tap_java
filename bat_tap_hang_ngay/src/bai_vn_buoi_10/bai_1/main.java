/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_vn_buoi_10.bai_1;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


/**
 *
 * @author Dell
 */
public class main {
    
    public static void menu(){
        System.out.println("1 : Them tu vao tu dien !");
        System.out.println("2 : Tra tu dien anh-viet , neu khong co thi tu them moi vao !");
        System.out.println("3 : Tra tu dien viet-anh , neu khong co thi tu them moi vao !");
        System.out.println("4 : Sap xep lai tu diem theo gia tri cua key !");
    }
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int choose ;
        HashMap<String , String> directory = new HashMap<>();
        do{
            menu();
            System.out.println("ban muon chon  : ");
            choose = sc.nextInt();
            sc.nextLine();
            switch(choose){
                case 1 : {
                    directory = addKey(directory);
                    break;
                }
                case 2 : {
                    directory = check_key(directory);
                    break;
                }
                case 3: {
                    directory = check_value(directory);
                    break;
                }
//                case 4: {
//                    sortWord(directory);
//                }
            }
        }while(choose != 5);
    }

    private static HashMap<String, String> addKey(HashMap<String, String> directory) {
        while(true){
            System.out.println("nhap tu tieng anh : ");
            String en = sc.nextLine();
            System.out.println("nhap nghia tieng viet cho tu vua nhap : ");
            String vn = sc.nextLine();
            directory.put(en, vn);
            System.out.println("ban muon nhap tiep khong (y/n) : ");
            String choose = sc.nextLine();
            if("n".equals(choose.toLowerCase())){
                break;
            }
        }
        return directory;
    }

    private static HashMap<String, String> check_key(HashMap<String, String> directory) {
        System.out.println("nhap tu tieng anh muon dich : ");
        String en = sc.nextLine();
        if(directory.containsKey(en)){
            System.out.println("tu " + en + " co nghia : " + directory.get(en));
        }
        else{
            addKey(directory);
        }
        return directory;
    }

    private static HashMap<String, String> check_value(HashMap<String, String> directory) {
        System.out.println("nhap tu tieng viet muon dich : ");
        String vn = sc.nextLine();
        if(directory.containsValue(vn)){
            directory.forEach((k,v) ->{
                if(v.equalsIgnoreCase(vn))
                    System.out.println("key: "+k+" value:"+v) ;
            });
        }
        else{
            addKey(directory);
        }
      
        return directory;
    }

//    private static void sortWord(HashMap<String, String> directory) {
//        ArrayList<String> list= new ArrayList<>();
//        HashMap<String , String> newDirectory =new HashMap<>();
//        directory.forEach((k , v)->{
//            list.add(k);
//        });
//        Collections.sort(list);
//        for(int i = 0;i< list.size() ; i++){
//            directory.forEach((k , v)->{
//               if( k == list.get(i) ){
//                   newDirectory.put(k , v);
//                   System.out.println("key : " + k + "value : " + v);
//               } 
//            });
//           
//        }
//        
//    }
}
