/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_9.bai_2;

/**
 *
 * @author Dell
 */
public class book {
    private int id_book;
    private int id_author;
    private String name_book;
    private int numOfPage;
    private String dateOfPush;
    private String puslisher;

    public book(int id_book, int id_author, String name_book, int numOfPage, String dateOfPush, String puslisher) {
        this.id_book = id_book;
        this.id_author = id_author;
        this.name_book = name_book;
        this.numOfPage = numOfPage;
        this.dateOfPush = dateOfPush;
        this.puslisher = puslisher;
    }

    public book() {
    }

    public int getId_book() {
        return id_book;
    }

    public void setId_book(int id_book) {
        this.id_book = id_book;
    }

    public int getId_author() {
        return id_author;
    }

    public void setId_author(int id_author) {
        this.id_author = id_author;
    }

    public String getName_book() {
        return name_book;
    }

    public void setName_book(String name_book) {
        this.name_book = name_book;
    }

    public int getNumOfPage() {
        return numOfPage;
    }

    public void setNumOfPage(int numOfPage) {
        this.numOfPage = numOfPage;
    }

    public String getDateOfPush() {
        return dateOfPush;
    }

    public void setDateOfPush(String dateOfPush) {
        this.dateOfPush = dateOfPush;
    }

    public String getPuslisher() {
        return puslisher;
    }

    public void setPuslisher(String puslisher) {
        this.puslisher = puslisher;
    }

    @Override
    public String toString() {
        return "book{" + "id_book=" + id_book + ", id_author=" + id_author + ", name_book=" + name_book + ", numOfPage=" + numOfPage + ", dateOfPush=" + dateOfPush + ", puslisher=" + puslisher + '}';
    }
    
    
    
    
}
