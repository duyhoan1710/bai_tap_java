/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_9.bai_2;

import java.util.*;

/**
 *
 * @author Dell
 */
 
public class main {
    private static Scanner sc = new Scanner(System.in);
    
    public static void menu(){
        System.out.println("1 : them 1 tac gia !");
        System.out.println("2 : them 1 cuon sach , kiem tra xem tac gia co ton tai khong !");
        System.out.println("3 : kiem tra 1 cuon sach bat ki xem co  ton tai khong !");
        System.out.println("4 : nhap ten 1 tac gia , in ra so sach cua tac gia ay !");
        System.out.println("5 : thoat !");
    }
    
    public static ArrayList<author> addAuthor(ArrayList<author> listAuthor){
        author author = new author();
        System.out.println("nhap thong tin cho tac gia !");
        System.out.println("ma tac gia : ");
        int idAuthor = sc.nextInt();
        author.setIdAuthor(idAuthor);
        sc.nextLine();
        System.out.println("ten tac gia : ");
        String name = sc.nextLine();
        author.setNameAuthor(name);
        listAuthor.add(author);
        return listAuthor;
    }  
    
    public static ArrayList<book> addBook(ArrayList<book> listBook ,ArrayList<author> listAuthor ){
        book book = new book();
        System.out.println("nhap thong tin cho sach !");
        System.out.println("ma scah : ");
        int idBook = sc.nextInt();
        book.setId_book(idBook);
        System.out.println("ma tac gia : ");
        int idAuthor = sc.nextInt();
        book.setId_author(idAuthor);
        sc.nextLine();
        System.out.println("ten sach : ");
        String nameBook = sc.nextLine();
        book.setName_book(nameBook);
        System.out.println("so trang sach : ");       
        int soTrang = sc.nextInt();
        book.setNumOfPage(soTrang);
        sc.nextLine();
        System.out.println("ngay xuat ban : ");
        String ngayXuatBan = sc.nextLine();
        book.setDateOfPush(ngayXuatBan);
        System.out.println("nha xuat ban : ");
        String nhaXuatBan = sc.nextLine();
        book.setPuslisher(nhaXuatBan);
        
        if(listAuthor.contains(idAuthor)){
            System.out.println("ma tac gia ton tai !");
            listBook.add(book);
        }
        else{
            addAuthor(listAuthor);
        }
        
        return listBook;
    }
    
    public static int check_book(ArrayList<book> listBook){
        int check = 0;
        System.out.println("nhap ten sach ban muon kiem tra : ");
        String book = sc.nextLine();
        if(listBook.contains(book)){
            check = 1;
        }
        return check;
    }
    
    public static int getIdAuthor(String name , ArrayList<author> listAuthor){
        int id = -1;
        for(int i = 0 ; i<listAuthor.size() ; i++){
            if(listAuthor.get(i).getNameAuthor() == name){
                id = listAuthor.get(i).getIdAuthor();
            }
        }
        return id;
    }
    
    public static void xuatDulieu (ArrayList<book> listBook , int id){
        for(int i =0 ;i< listBook.size() ; i++){
            if(listBook.get(i).getId_author() == id){
                System.out.println(listBook.get(i).toString());
            }
        }
    }
    
    public static void main(String[] args) {
        menu();
        int choose;
        
        ArrayList<author> listAuthor = new ArrayList<>();
        ArrayList<book> listBook = new ArrayList<>();
        
        do{
            choose = sc.nextInt();
            switch(choose){
                case 1 : {
                    listAuthor = addAuthor(listAuthor);
                    break;
                }
                
                case 2 : {
                   listBook = addBook(listBook , listAuthor);
                   break;
                }
                case 3 :{
                    int check = check_book(listBook);
                    if(check == 1){
                        System.out.println("cuon sach co thuoc trong danh sach !");
                        
                    }
                    else{
                        System.out.println("cuon sach do khong thuoc trong danh sach !");
                    }
                    break;
                }
                
                case 4 :{
                    System.out.println("nhap ten tac gia : ");
                    String name = sc.nextLine();
                    int id = getIdAuthor(name , listAuthor);
                    xuatDulieu(listBook , id);
                    break;
                }
                
                
            }
        }while(choose != 5);
        
        
    }
            
}
