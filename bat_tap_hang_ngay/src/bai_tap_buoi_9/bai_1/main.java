/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_9.bai_1;

import java.util.*;

/**
 *
 * @author Dell
 */
public class main {
    private static Scanner sc = new Scanner(System.in);
    public static void menu(){
        System.out.println("1 : them 1 sinh vien vao danh sach !");
        System.out.println("2 : hien thi danh sach sinmh vien !");
        System.out.println("3 : hien thi danh sach sinh vien mang gioi tinh nu !");
        System.out.println("4 : hien thi danh sach sin h vien ten tuan !");
        System.out.println("5 : nhap ten sinh vien  va kiem tra xem sinh vien  do cos thuoc danh sach hay khong !");
    }
    
    public static ArrayList<Student> nhap_tt(ArrayList<Student> listStudent){
        Student st = new Student();
        System.out.println("nhap thong tin cho sinh vien !");
        System.out.println("ma sinh vien : ");
        int id = sc.nextInt();
        st.setId(id);
        sc.nextLine();
        System.out.println("ho ten sinh vien : ");
        String name = sc.nextLine();
        st.setName(name);
        System.out.println("tuoi : ");
        int age = sc.nextInt();
        st.setAge(age);
        sc.nextLine();
        System.out.println("gioi tinh ; ");
        String gioiTinh = sc.nextLine();
        st.setGender(gioiTinh);
        System.out.println("que quan : ");
        String adress = sc.nextLine();
        st.setHometown(adress);
        listStudent.add(st);
        return listStudent;
    }
    
    public static void inData(ArrayList<Student> listStudent){
        for(int i =0 ;i<listStudent.size() ; i++){
            System.out.println(listStudent.get(i).toString());
        }
    }
    
    public static void hienThiDSNu(ArrayList<Student> listStudent){
        for(int i = 0; i< listStudent.size() ; i++){
            if(listStudent.get(i).getGender() == "nu"){
                System.out.println(listStudent.get(i));
            }
        }
    }
    
    public static void hienThiDSTuan(ArrayList<Student> listStudent){
        for(int i = 0; i< listStudent.size() ; i++){
            if(listStudent.get(i).getName().toLowerCase() == "tuan"){
                System.out.println(listStudent.get(i));
            }
        }
    }
    
    public static ArrayList<Student> check_student(ArrayList<Student> listStudent , String name){
        int check = 0;
        for(int i = 0; i< listStudent.size() ; i++){
            if(listStudent.get(i).getName() == name){
                check =1;
                System.out.println(listStudent.get(i));
            }
        }
        if(check == 0){
            nhap_tt(listStudent);
        }
        return listStudent;
    }
    
    public static void main(String[] args) {
        ArrayList<Student> listStudent = new ArrayList<>();
        
        menu();
        int choose ;
        do{
           choose = sc.nextInt();
           switch(choose){
               case 1 : {
                   listStudent = nhap_tt(listStudent);
                   break;
               }
               case 2 :{
                   inData(listStudent);
                   break;
               }
               
               case 3 :{
                   hienThiDSNu(listStudent);
                   break;
               }
               case 4 :{
                   hienThiDSTuan(listStudent);
                   break;
               }
               case 5 :{
                   System.out.println("nhap ten sinh vien muon kiem tra : ");
                   String name = sc.nextLine();
                   check_student(listStudent , name);
                   
               }
           }
        }while(choose != 6);
    }   
        
}
