/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_9.bai_1;

/**
 *
 * @author Dell
 */
public class  Student{
    private int id ; 
    private String name;
    private int age;
    private String gender;
    private String hometown;

    public Student(int id, String name, int age, String gender, String hometown) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.hometown = hometown;
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", hometown=" + hometown + '}';
    }
    
    
    
}
