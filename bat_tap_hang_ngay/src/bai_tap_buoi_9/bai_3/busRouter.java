/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap_buoi_9.bai_3;

import java.util.*;

/**
 *
 * @author Dell
 */
public class busRouter {
    private int idBus ; 
    private int numOfBus;
    private ArrayList<String> listRoad;

    public busRouter(int idBus, int numOfBus, ArrayList<String> listRoad) {
        this.idBus = idBus;
        this.numOfBus = numOfBus;
        this.listRoad = listRoad;
    }

    public busRouter() {
    }

    public int getIdBus() {
        return idBus;
    }

    public void setIdBus(int idBus) {
        this.idBus = idBus;
    }

    public int getNumOfBus() {
        return numOfBus;
    }

    public void setNumOfBus(int numOfBus) {
        this.numOfBus = numOfBus;
    }

    public ArrayList<String> getListRoad() {
        return listRoad;
    }

    public void setListRoad(ArrayList<String> listRoad) {
        this.listRoad = listRoad;
    }

    @Override
    public String toString() {
        return "busRouter{" + "idBus=" + idBus + ", numOfBus=" + numOfBus + ", listRoad=" + listRoad + '}';
    }
    
    
}
